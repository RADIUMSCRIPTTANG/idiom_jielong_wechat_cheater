from pypinyin import lazy_pinyin
import uiautomation as auto
import pickle
import random
import time

d = pickle.load(open('idiom.pk',"rb"))
for i in range(30):
    print('\r[' + '='*i + '.'*(30-i) + ']',end='',flush=True)
    time.sleep(0.1)

wechat_window = auto.WindowControl(ClassName='WeChatMainWndForPC',searchDepth=1)
message_list_window = wechat_window.ListControl(Name='消息')

last_message = ""
while True:
    if last_message != message_list_window.GetChildren()[-1].Name:
        last_message = message_list_window.GetChildren()[-1].Name
        last_message_pinyin = lazy_pinyin(last_message)[-1]
        res = random.sample(d[last_message_pinyin],1)[0]
        auto.SendKeys(res)
        auto.SendKeys('{enter}')
        last_message = message_list_window.GetChildren()[-1].Name
    else:
        time.sleep(1)

