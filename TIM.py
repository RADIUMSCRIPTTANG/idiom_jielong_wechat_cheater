import uiautomation as auto
from pypinyin import lazy_pinyin
import pickle
import time
import random

print("正在读取成语库，请稍后")
d = pickle.load(open("idiom.pk","rb"))

chat_name = input("请输入准确的聊天名称")
my_name = input("请输入我在群聊中的名称，以防止接龙自己")
print("程序将在3秒后启动，请讲TIM置顶并将光标锁定输入框")
for i in range(30):
    print('\r'+'='*i + '>' + '.'*(30- i - 1), end='', flush=True)
    time.sleep(0.1)


qq_ui = auto.WindowControl(searchDepth=1, ClassName="TXGuiFoundation")
fulian_window = qq_ui.ListItemControl(Name=chat_name)
last = ""
res = "。。"
while True:
    if last != fulian_window.GetLegacyIAccessiblePattern().Value:

        last = fulian_window.GetLegacyIAccessiblePattern().Value
        if last.find(my_name) > -1 or res.find(fulian_window.GetLegacyIAccessiblePattern().Value) > -1:
            continue
        print(last)
        try:
            py_last = lazy_pinyin(last[-1])[0]
            res = random.sample(d[py_last], 1)[0]
            print(res)
            auto.SendKeys(res)
            auto.SendKeys('{enter}')
        except:
            pass
    else:
        time.sleep(1)